# My project's README

### Basic android application

#### When you clone this application, follow the below steps

update the package name with your package name
update the single-ton class name as per your project need (application -> SampleApplication)
update the SampleConstants name as per your project need (utils -> SampleConstants)

##### For your Information:
I am using Volley parser to parse the API response. Based on that I have used dependency in gradle file.


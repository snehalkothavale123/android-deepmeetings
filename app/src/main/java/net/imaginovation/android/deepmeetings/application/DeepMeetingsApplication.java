package net.imaginovation.android.deepmeetings.application;
import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by Santosh on 4/17/2018.
 */
public class DeepMeetingsApplication extends Application {
    private static final String TAG = DeepMeetingsApplication.class.getSimpleName();
    private static final String BASE_URL = "https://dwevans-portal-dev.dev-imaginovation.net:8443/";
    private static DeepMeetingsApplication deepMeetingsApplication;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;
    private String userToken, userID, uName, userType, clientId, usertype1;

    public static synchronized DeepMeetingsApplication getInstance() {
        return deepMeetingsApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        deepMeetingsApplication = this;
    }

    /**
     * Singleton request queue.
     *
     * @return
     */
    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return requestQueue;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUsertype1() {
        return usertype1;
    }

//    public ImageLoader getImageLoader() {
//        getRequestQueue();
//        if (imageLoader == null) {
//            imageLoader = new ImageLoader(this.requestQueue,
//                    new LruBitmapCache());
//        }
//        return this.imageLoader;
//    }

    public void setUsertype1(String usertype1) {
        this.usertype1 = usertype1;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }


}


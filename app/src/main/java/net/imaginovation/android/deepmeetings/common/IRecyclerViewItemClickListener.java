package net.imaginovation.android.deepmeetings.common;

import android.view.View;

/**
 * Created by Santosh on 4/17/2018.
 */

public interface IRecyclerViewItemClickListener {
    void onItemClicked(View v, int position);
}

package net.imaginovation.android.companyandroidapp.util.font;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;


/**
 * Created by Santosh on 4/17/2018.
 */

public class EditTextPtSansBold extends AppCompatEditText {
    public EditTextPtSansBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isInEditMode()) {
            return;
        }
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/pt-sans.bold.ttf");
        setTypeface(typeface);
    }
}

package net.imaginovation.android.deepmeetings.Login.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import net.imaginovation.android.deepmeetings.R;
import net.imaginovation.android.deepmeetings.common.BaseFragment;
import net.imaginovation.android.deepmeetings.util.CommonUtils;
import net.imaginovation.android.deepmeetings.util.SampleConstants;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * Created by Santosh on 4/17/2018.
 */
public class UserLoginFragment extends BaseFragment {


    @BindView(R.id.forgotTextView)
    TextView forgotPassword;
    @BindView(R.id.et_username)
    TextView userName;
    @BindView(R.id.et_password)
    TextView userPassword;
    @BindView(R.id.cb_remember_me)
    CheckBox rememberMe;
    private UserLoginActivity userLoginActivity;
    //private UserLoginPresenter userLoginPresenter;
    private boolean flag;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        bindViews(view);
        preFillLoginCredentials();
        // userLoginPresenter = new UserLoginPresenter(this);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        userLoginActivity = (UserLoginActivity) context;
    }

    /**
     * In this method on click of Forgot Password text will navigate you to ResetPasswordActivity
     */
    @OnClick(R.id.rl_forgotTextView)
    public void onClickForgotPassword() {

//        Intent intent = new Intent(userLoginActivity, ForgotPassword.class);
//        userLoginActivity.finish();
//        startActivity(intent);
        //Hide keypad if open
      /*  if (userName.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomSnackBar(getView(), "Please enter email id");
        } else {
            CommonUtils.hideKeypad(userLoginActivity);
            CommonUtils.showProgressDialog(userLoginActivity);
            userLoginPresenter.callPutForgotPasswordAPIViaEmail(userName.getText().toString().trim());
        }
*/
      /*  Intent intent = new Intent(userLoginActivity, ResetPasswordActivity.class);
        startActivity(intent);*/

    }

    /**
     * In this method on click of Submit button will navigate you to Client user home screen
     */
    @OnClick(R.id.btn_user_login)
    public void onClickLoginButton() {
        CommonUtils.hideKeypad(userLoginActivity);
        if (rememberMe.isChecked()) {
            flag = true;
        } else {
            flag = false;
        }
//        CommonUtils.showProgressDialog(userLoginActivity);
//        //API call for login
//        userLoginPresenter.callPostLoginAPI(userName.getText().toString().trim(), userPassword.getText().toString().trim());

    }

    /**
     * Save login data in shared preferences.
     *
     * @param email
     * @param password
     */
    public void saveLoginCredentials(String email, String password) {
        if (flag) {
            SharedPreferences sharedPreferences = userLoginActivity.getSharedPreferences(SampleConstants.PREFERENCES,
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(SampleConstants.OWNER_EMAIL, email);
            editor.putString(SampleConstants.OWNER_PASSWORD, password);
            editor.commit();
        } else {
            SharedPreferences sharedPreferences = userLoginActivity.getSharedPreferences(SampleConstants.PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            if (sharedPreferences.contains(SampleConstants.OWNER_PASSWORD)) {
                editor.remove(SampleConstants.OWNER_PASSWORD);
                editor.commit();
            }
        }
    }

    /**
     * Load data from shared preferences.
     */
    private void preFillLoginCredentials() {
        SharedPreferences sharedPreferences = userLoginActivity.getSharedPreferences(SampleConstants.PREFERENCES,
                Context.MODE_PRIVATE);
        String email = sharedPreferences.getString(SampleConstants.OWNER_EMAIL, "");
        String password = sharedPreferences.getString(SampleConstants.OWNER_PASSWORD, "");
        if (password.length() != 0 && password != null) {
            rememberMe.setChecked(true);
        }
        userName.setText(email);
        userPassword.setText(password);
    }


    /**
     * On success of login going to user home activity
     */

    public void gotoClientHome() {
        CommonUtils.hideProgressDialog();
        //Intent intent = new Intent(userLoginActivity, UserHomeActivity.class);
        //intent.putExtra(DWEvansConstants.NEW_USER, false);
        userLoginActivity.finish();
        //  startActivity(intent);
    }


}

package net.imaginovation.android.deepmeetings.Login.presenter;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import net.imaginovation.android.deepmeetings.application.DeepMeetingsApplication;
import net.imaginovation.android.deepmeetings.util.CommonUtils;
import net.imaginovation.android.deepmeetings.util.api.APIConstants;
import net.imaginovation.android.deepmeetings.util.api.VolleyErrorHelper;
import net.imaginovation.android.deepmeetings.Login.view.UserLoginActivity;
import net.imaginovation.android.deepmeetings.Login.view.UserLoginFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Santosh on 4/17/2018.
 */

public class UserLoginPresenter {
    private static final String TAG = UserLoginPresenter.class.getSimpleName();
    private UserLoginActivity userLoginActivity;
    private UserLoginFragment fragment;

    /**
     * Constructor.
     */
    public UserLoginPresenter(UserLoginFragment fragment) {
        userLoginActivity = (UserLoginActivity) fragment.getActivity();
        this.fragment = fragment;
    }

    /**
     * Call login API.
     */
    public void callPostLoginAPI(final String username, final String password) {
        //Create jsonObject params
        JSONObject params = new JSONObject();
        try {
            params.put("email", username);
            params.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        Log.e(TAG, "callPostLoginAPI params: " + params.toString());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                APIConstants.USER_LOGIN, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        Log.d(TAG, "callPostLoginAPI response "+ response.toString());
//                        if (response.optBoolean("status")) {
//
//                            String token = response.optString("token");
//                            String userId = response.optString("UserID");
//                            String uName = response.optString("name");
//
//                            SampleApplication.getInstance().setUserToken(token);
//                            SampleApplication.getInstance().setUserID(userId);
//                            SampleApplication.getInstance().setuName(uName);
//                            try {
//                                if (response.getJSONObject("attributes").optString("usertype").equalsIgnoreCase("certek")) {
//                                    SampleApplication.getInstance().setUsertype1(response.getJSONObject("attributes").optString("usertype"));
//                                    SampleApplication.getInstance().setUserType(response.getJSONObject("attributes").optString("usertype"));
//                                    fragment.gotoCertekHome();
//                                } else {
//                                    String clientId = response.optString("clientid");
//                                    DWEvansApplication.getInstance().setUsertype1(response.getJSONObject("attributes").optString("usertype"));
//                                    DWEvansApplication.getInstance().setClientId(clientId);
//                                    DWEvansApplication.getInstance().setUserType(response.getJSONObject("attributes").optString("usertype"));
//                                    fragment.gotoClientHome();
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
                        fragment.saveLoginCredentials(username, password);


                        CommonUtils.hideProgressDialog();
                        CommonUtils.showAlertDialog(userLoginActivity, response.optString("message").trim());


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "callPostLoginAPI error " + error.toString());
                String msg = VolleyErrorHelper.getresponse(error, userLoginActivity);
                //Hide Progress bar
                CommonUtils.hideProgressDialog();
                CommonUtils.showAlertDialog(userLoginActivity, msg);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };

        // add the request object to the queue to be executed
        DeepMeetingsApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

}

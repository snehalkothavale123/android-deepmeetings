package net.imaginovation.android.deepmeetings.Login.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import net.imaginovation.android.deepmeetings.R;
import net.imaginovation.android.deepmeetings.common.BaseActivity;


/**
 * Created by Santosh on 4/17/2018.
 */

public class UserLoginActivity extends BaseActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default);

        bindViews();

        UserLoginFragment userLoginFragment = new UserLoginFragment();
        addFragment(userLoginFragment, getString(R.string.login), false);

    }

    /**
     * Add fragment to this activity.
     *
     * @param fragment       class to add
     * @param tag            tag for the fragment
     * @param addToBackStack true if need to add to backStack, false otherwise.
     */
    public void addFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.container, fragment, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
